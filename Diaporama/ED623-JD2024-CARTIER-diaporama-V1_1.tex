% Created 2024-03-28 jeu. 22:02
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{beamer}
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{wasysym}
\usepackage{setspace}
\newcommand{\fname}[1]{\textsc{#1}}
\usetheme{default}
\author{Clément Cartier}
\date{\textit{<2024-03-13 mer.>}}
\title{Enseignement instrumenté des mathématiques: le cas de Richard de Wallingford}
\institute{SPHERE\\ ED 623: Savoirs, Sciences, Éducation}
\usepackage{caption}
\usepackage{subcaption}
\usetheme{upcite}
\AtBeginSection[]{\begin{frame}<beamer>\frametitle{Plan}\tableofcontents[currentsection, currentsubsection]\end{frame}}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Enseignement instrumenté des mathématiques: le cas de Richard de Wallingford},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle

\section{Qu'est-ce que l'Université d'Oxford}
\label{sec:orge853826}

\begin{frame}[label={sec:org714e5ee}]{Université et universel}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.6\paperheight]{../img/New_College-2755-painting-1923-BRILEY.jpg}
\caption{Hastings Rashdall (1858--1924), peinture à l'huile par Oswald Briley en 1923 (New College, 2755. Photographie: Bridgeman Images ©CC-BY)}
\end{figure}
\end{column}


\begin{column}{.5\columnwidth}
\begin{block}{(\citeprocitem{23}{Rashdall, 1895} p.6)}
\begin{quote}
The word \emph{Universitas} is one to which a false explanation is often assigned for polemical purposes by controversial writers […]. \alert{The notion that a University means a \emph{Universitas Facultatum} --- a School in which all the Faculties or Branches of knowledge are represented ---} has, indeed, long since disappeared from the pages of professed historians; but it \alert{is still persistently foisted upon the public by writers with whom history is subordinate to what may be called intellectual edification}.
\end{quote}
\end{block}
\end{column}
\end{columns}
\end{frame}


\begin{frame}[label={sec:org3a3fa46}]{\emph{Universitas}}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{block}{(\citeprocitem{23}{Rashdall, 1895} p.7)}
\begin{quote}
A glance into any collection of medieval documents reveals the fact that \alert{the word \og University\fg{} means merely a number, a plurality, an aggregate of persons}. \emph{Universitas vestra}, in a letter addressed to a body of persons, means merely \og the whole of you\fg{}; in a more technical sense \alert{it denotes a legal corporation} […].
\end{quote}
\end{block}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Paris-BNF-Monnaies-MATR_119.jpg}
\caption{Sceau de l'université de Paris accordé en 1246. On y lit \emph{Universitas magistrorium et scolarium} (Paris, Bibliothèque Nationale de France, Monnaies et médailles antiques, MATR 119 - ©Etalab)}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org2ef16ab}]{Les corporations}
\begin{columns}
\begin{column}{.5\columnwidth}
Étienne Boileau, \emph{Le Livre des métiers} (c. 1266-1269) - voir (\citeprocitem{17}{Lespinasse, 1886}, \citeprocitem{18}{Lespinasse \& Bonnardot, 1879}):

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Paris-BNF-Fr_24069-fer.jpg}
\caption{\og C'est l'ordonnance de l'intitulement des registres des métiers et marchandises de la ville de Paris qui sont escripts en ce livre par chapitres et nombres et aussi sont nombres les foilles du livres pour ce que chascun mestier [et] marchandise nest pas aparsoy ancoirs est en plusiers feuillez [et] parties.\fg{} ( Paris, Bibliothèque Nationale de France, Français 24069, f. e recto - ©Etalab)}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.3\paperheight]{../img/Wikimedia-Paris_Hotel_de_Ville_la_galerie_des_glaces_(2)-PIERRE_ANDRE_LECLERCQ-2011.jpg}
\caption{Blasons de corporations à l'entrée du salon des arcades de l'hôtel de ville de Paris par Gabriel Léglise, réalisés en 1935 (Image: Pierre André Leclercq sur Wikimedia commons, 2011, ©CC-BY-SA)}
\end{figure}
\end{column}
\end{columns}

\begin{block}{Références}
\begin{itemize}
\item (\citeprocitem{5}{Branthôme, 2013}) (\citeprocitem{13}{Jéhanno, 2015}) (\citeprocitem{1}{Anheim, 2013})
\end{itemize}
\pause
\begin{itemize}
\item (\citeprocitem{15}{Le Goff, 1977}) (\citeprocitem{6}{Braunstein, 2003})
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[label={sec:org01f893b}]{Sources}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.4\paperheight]{../img/Paris-BIS-MSAUC_7_D10b.jpg}
\caption{Statuts --- ici ceux de l'université de Paris promulgués par Rorbert de Courçon en 1215 (Paris, Bibliothèque interuniversitaire de la Sorbonne, MSAUC 7 D.10.b) - voir (\citeprocitem{11}{Genet, 1997}, \citeprocitem{12}{Gibson, 1931})}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.4\paperheight]{../img/Paris-BNF-Latin_16609-f30r.jpg}
\caption{Raoul le Breton, \emph{Questiones mathematice} (Paris, Bibliothèque Nationale de France - Latin 16609, f.30r) - voir (\citeprocitem{24}{Weijers, 1995}, \citeprocitem{14}{Lafleur, Weijers, \& Holtz, 1997})}
\end{figure}
\end{column}
\end{columns}

\begin{block}{Mais aussi\ldots{}}
\begin{columns}
\begin{column}{.6\columnwidth}
(\citeprocitem{11}{Genet, 1997})
\begin{itemize}
\item Archives: \emph{Gesta Abbatum Monastrii Sancti Albani}, Meton College
\item Bases de données prosopographiques / bibliographiques
\item Manuscrits + éditions numériques
\end{itemize}

\pause
\end{column}
\begin{column}{.3\columnwidth}
\begin{itemize}
\item Iconographie
\item Données archéologiques
\end{itemize}
\end{column}
\end{columns}
\end{block}
\end{frame}

\section{Cursus d'un étudiant Oxfonien}
\label{sec:org8f4d513}

\subsection{Enseignement élémentaire}
\label{sec:org9d07359}

\begin{frame}[label={sec:orgaf6b463}]{Origines de Richard de Wallingford}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{center}
\includegraphics[height=.6\paperheight]{/tmp/babel-TY2xid/local_mapE59Pu7.png}
\end{center}

\pause
\end{column}

\begin{column}{.5\columnwidth}
\begin{block}{(\citeprocitem{20}{North, 1976} p.95)}
\begin{quote}
It is easier still to overlook the difficulty whith which a great many clerks read Latin and, \emph{a fortiori}, thought in the language. […] Archbishop Pecham is found complaining that, despite Walter of Merton's well-laid schemes, \alert{the rule of Latin conversation was being constantly broken at Merton College} ---perhaps a less culpable failure, but one which at least suggests that \alert{Latin did not easily become a language for creative thought}. […] Not surprisingly, vernacular writings flowed in an everquickening stream from the ink-horns of the fourteenth-century, \alert{occasionnaly with a sentence or two of apology, or even of hostility towards the artificial language of the Schools}.
\end{quote}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orgaf3a7f4}]{Enseignement élémentaire}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{center}
\includegraphics[height=.7\paperheight]{../img/Falk-Light_Ages-cover.png}
\end{center}
\end{column}


\begin{column}{.5\columnwidth}
\begin{block}{(\citeprocitem{8}{Falk, 2020} p.43)}
\begin{quote}
No beginner's primer survives from the medieval period, but the mathematical texts that were popular in monasteries start from the assumption that their readers had already attained that elementary level.
\end{quote}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org2eb8586}]{Monocorde}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}
\begin{center}
\includegraphics[width=.3\textwidth]{../img/Bailly-Psalterion-2011-B64.png}
\includegraphics[width=.3\textwidth]{../img/Bailly-Psalterion-2011-B66.png}
\includegraphics[width=.3\textwidth]{../img/Bailly-Psalterion-2011-B67.png}
\end{center}
\caption{Psaltérions de la cathédrale de Reims (\citeprocitem{2}{Bailly, 2011})}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{block}{\emph{Scriptores ecclesiastici de musica}}
\begin{quote}
\emph{Sicut magister omnes tibi litteras primum ostendit in tabula, ita et musicus omnes cantilenae voces in monochordo insinuat}.
(\citeprocitem{4}{Beaujouan, 1991} I-p.614)
\end{quote}
\end{block}

\begin{block}{Ma traduction}
\begin{quote}
Alors que le maître vous présente d'abord toutes les lettres dans un tableau, de même le musicien introduit toutes les notes du chant avec le monocorde.
\end{quote}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orge1cd10b}]{Main Guidonienne}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Milan-Veneranda_Biblioteca_Ambrosiana-D_75_inf-fc3v.jpg}
\caption{Main Guidonienne (Milan, Veneranda Biblioteca Ambrosiana, D 75 inf, f.c3v - pas d'information de licence)}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Paris-BNF-La_7418-ff3v_4r.jpg}
\caption{(Paris, Bibliothèque Nationale de France, Latin 7418 - ©Etalab)}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\subsection{La faculté des arts}
\label{sec:orgdcb2815}

\begin{frame}[label={sec:org13645dc}]{Gloucestershire College}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/map-oxford-1375-WASS.png}
\caption{Carte d'Oxford par K. J. Wass à partir d'un brouillon de Janet Cooper et des informations de Julian Munby (\citeprocitem{7}{Crossley, 1979})}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Flickr-Oxford_St_Mary-NICHOLLS-2013.jpg}
\caption{Église universitaire Sainte Marie dans la High Street à Oxford (Image: David Nicholls sur flickr, mars 2013 ©CC-BY-NC)}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org522f1cb}]{Parcours à la faculté des arts}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/DATA-Gloucester-EDT-sophiste-octobre-V1_0.png}
\caption{Emploi du temps fictif de Richard de Wallingford en tant que sophiste de la Faculté des arts (1308--1311), puis bachelier après sa \emph{déterminatio} (1311--1314) -- voir (\citeprocitem{12}{Gibson, 1931} p.lxxxi)(\citeprocitem{9}{Fletcher, 1984} pp.375-384)}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.5\paperheight]{../img/Paris-BNF-La_7432-f160r.jpg}
\caption{Incipit de l'\emph{Introduction au jugement des astres} d'Ali al-Kabisi par Conrad Heingarter (Paris, Bibliothèque Nationale de France, Latin 7432, f.160r - ©Etalab)}
\end{figure}
\end{column}
\end{columns}
\begin{block}{Arts libéraux}
\begin{description}
\item[{\emph{Trivium}}] Logique, Grammaire, Rhétorique (\citeprocitem{19}{Lewry, 1984})(\citeprocitem{23}{Rashdall, 1895} pp.433-435)
\item[{\emph{Quadrivium}}] Arithmétique, Musique, Géométrie, Astronomie (\citeprocitem{3}{Beaujouan, 1997}, \citeprocitem{16}{Lejbowicz, 1997})
\end{description}
\end{block}
\end{frame}




\begin{frame}[label={sec:org79a985f}]{Sophiste à la faculté de théologie}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/DATA-Gloucester-EDT-regent-octobre-V1_0.png}
\caption{Emploi du temps fictif de Richard de Wallingford après son retour à Oxford  (1317--1321)}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/DATA-Gloucester-EDT-sophiste_theologie-octobre-V1_0.png}
\caption{Emploi du temps fictif de Richard de Wallingford après ses quatre premières années d'étude de la Bible (1321--1323)}
\end{figure}
\end{column}
\end{columns}

\begin{block}{References}
(\citeprocitem{23}{Rashdall, 1895} pp.464-465)(\citeprocitem{10}{Genet, 1999} pp.220-222)
\end{block}
\end{frame}

\begin{frame}[label={sec:orgdd706f0}]{Bachelier à la faculté de théologie}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/DATA-Gloucester-EDT-cursor-octobre-V1_0.png}
\caption{Emploi du temps fictif de Richard de Wallingford après son \emph{incipium} (1323--1325), puis en tant que sente}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{description}
\item[{1323}] \emph{incipium}, bachelier Cursor (1323-1325)
\item[{1325}] \emph{tentative}, bachelier Sententiaire (1325-1327)
\item[{1327}] \emph{incipium}, bachelier Formatus
\end{description}
\end{column}
\end{columns}
\end{frame}

\section{\emph{Cursus astronomicum}}
\label{sec:org9c49348}

\begin{frame}[label={sec:orgcbe82fb}]{Textes de référence}
\begin{columns}
\begin{column}{.5\columnwidth}
(\citeprocitem{21}{Pedersen, 1973})

\begin{block}{Sacrobosco, Traité de la Sphère}
\begin{columns}
\begin{column}{.4\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Paris-BNF-NAL_1893-f19r.jpg}
\caption{Représentation des climats dans le traité de la Sphère (Paris, BNF, NAL 1893, f.19r - ©Etalab)}
\end{figure}
\end{column}

\begin{column}{.4\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Paris-BNF-NAL_1893-f22v.jpg}
\caption{Théorie des éclipses (Paris, BNF, NAL 1893, f.22v)}
\end{figure}
\end{column}
\end{columns}
\end{block}
\end{column}

\begin{column}{.5\columnwidth}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.2\paperheight]{../img/Paris-BNF-NAL_1893-ff44v_45r.jpg}
\caption{Quadrant moderne (Paris, BNF, NAL 1893, ff.44v-45r)}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.2\paperheight]{../img/Paris-BNF-NAL_1893-ff49v_50r.jpg}
\caption{Construction de l'astrolabe (Paris, BNF, NAL 1893, ff.49v-50r)}
\end{figure}
\end{column}
\end{columns}

\begin{block}{Campanus, Theorice planetarum}
\begin{columns}
\begin{column}{.4\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.2\paperheight]{../img/Paris-BNF-NAL_1893-f66r.jpg}
\caption{Théorie de la Lune (Paris, BNF, NAL1893, f.66r)}
\end{figure}
\end{column}

\begin{column}{.4\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.2\paperheight]{../img/POULLE-Equatoires2-1980-fig10-p894.png}
\caption{Équatoire pour la Lune (\citeprocitem{22}{Poulle, 1980})}
\end{figure}
\end{column}
\end{columns}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org3d7d5b9}]{Astrologie}
\begin{columns}
\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.6\paperheight]{../img/Munich-BSB-Clm10268-f94v.png}
\caption{Illustration de l'homme zodiacal dans Michael Scot, \emph{Liber introductorius} (Munich, Bayerische Staatsbibliothek, Clm 10268, f.94v - ©CC-BY-SA)}
\end{figure}
\end{column}

\begin{column}{.5\columnwidth}
\begin{figure}[htbp]
\centering
\includegraphics[height=.6\paperheight]{../img/Paris-BNF-La_7432-f103v.jpg}
\caption{Incipit du \emph{Quadripartitum} de Ptolémé par Conrad Hemgarter (Paris, Bibliothèque Nationale de France, Latin 7432, f.103v - ©Etalab)}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\section{Bibliographie}
\label{sec:orge5c092a}

\begin{frame}[allowframebreaks]{Bibliographie}
\hypertarget{citeproc_bib_item_1}{[1] Étienne Anheim, \href{https://www.cairn.info/revue-annales-2013-4-page-1027.htm}{« Les hiérarchies du travail artisanal au Moyen Âge entre histoire et historiographie »} \textit{Annales. Histoire, Sciences Sociales}. 2013 (fr-FR).}

\hypertarget{citeproc_bib_item_2}{[2] Laure Bailly, \href{https://www.instrumentariumdechartres.fr/data/IndeXysBibliothequeHTML/7e54cc4a-8566-49f9-8faf-e7ab71062779/Reims/Memoire-L.-Bailly-Texte.pdf}{« Les représentations des instruments de musique dans le programme iconographique de la cathédrale de Reims »} 2011 (fr-FR, Mémoire de master 2).}

\hypertarget{citeproc_bib_item_3}{[3] Guy Beaujouan, \href{https://doi.org/10.1484/M.SA-EB.4.00381}{« Le quadrivium et la Faculté des arts »} Olga Weijers \& Louis Holtz (éd.) \textit{L’enseignement des disciplines à la Faculté des arts (Paris et Oxford, XIIIe-XVe siècles)}. 1997.}

\hypertarget{citeproc_bib_item_4}{[4] Guy Beaujouan, \textit{Par raison de nombres: l’art du calcul et les savoirs scientifiques médiévaux} 1991.}

\hypertarget{citeproc_bib_item_5}{[5] Thomas Branthôme, \href{https://doi.org/10.3917/etsoc.157.0213}{« Introduction à l’historiographie des corporations : une histoire polémique (1880-1945) »} \textit{Les Études Sociales}. 2013 (fr-FR).}

\hypertarget{citeproc_bib_item_6}{[6] Philippe Braunstein, \textit{Travail et entreprise au Moyen Âge} 2003.}

\hypertarget{citeproc_bib_item_7}{[7] \textit{\href{https://www.british-history.ac.uk/vch/oxon/vol4}{A History of the County of Oxford. 4: The City of Oxford}} Alan Crossley (éd.). 1979 (en-US).}

\hypertarget{citeproc_bib_item_8}{[8] Seb Falk, \textit{The light ages: the surprising story of medieval science} 2020.}

\hypertarget{citeproc_bib_item_9}{[9] J M Fletcher, « The Faculty of Arts » Jeremy Catto (éd.) \textit{The History of the University of Oxford. The Early Oxford Schools}. 1984 (en-US).}

\hypertarget{citeproc_bib_item_10}{[10] Jean-Philippe Genet, \textit{La mutation de l’éducation et de la culture médiévale. Occident chrétien (XIIe-milieu du XVe siècle)} 1999 (fr-FR).}

\hypertarget{citeproc_bib_item_11}{[11] Jean-Philippe Genet, \href{https://doi.org/10.1484/M.SA-EB.4.00376}{« Rapport de la table ronde sur le cadre institutionnel »} Olga Weijers \& Louis Holtz (éd.) \textit{L’enseignement des disciplines à la Faculté des arts (Paris et Oxford, XIIIe-XVe siècles)}. 1997.}

\hypertarget{citeproc_bib_item_12}{[12] \textit{Statuta antiqua universitatis oxoniensis} Strickland Gibson (éd.). 1931.}

\hypertarget{citeproc_bib_item_13}{[13] Christine Jéhanno, \href{https://doi.org/10.4000/medievales.7567}{« Le travail au Moyen Âge, à Paris et ailleurs : retour sur l’histoire d’un modèle »} \textit{Médiévales. Langues, Textes, Histoire}. 2015 (fr-FR).}

\hypertarget{citeproc_bib_item_14}{[14] \href{https://doi.org/10.1484/M.SA-EB.5.106201}{« Les textes «didascaliques» («introductions à la philosophie» et «guides de l’étudiant») de la Faculté des arts de Paris au XIIIe siècle: notabila et status quaestionis »} Claude Lafleur, Olga Weijers, \& Louis Holtz (éd.) \textit{L’enseignement des disciplines à la Faculté des arts (Paris et Oxford, XIIIe-XVe siècles)}. 1997 (fr-FR).}

\hypertarget{citeproc_bib_item_15}{[15] Jacques Le Goff, \textit{Pour un autre Moyen Âge. Temps, travail et culture en Occident} 1977.}

\hypertarget{citeproc_bib_item_16}{[16] Max Lejbowicz, \href{https://doi.org/10.1484/M.SA-EB.4.00382}{« Les disciplines du quadrivium: L’astronomie »} Olga Weijers \& Louis Holtz (éd.) \textit{L’enseignement des disciplines à la Faculté des arts (Paris et Oxford, XIIIe-XVe siècles)}. 1997.}

\hypertarget{citeproc_bib_item_17}{[17] René Lespinasse, \textit{\href{https://gallica.bnf.fr/ark:/12148/bpt6k5823044k}{Les métiers et corporations de la ville de Paris : XIVe-XVIIIe siècles. Ordonnances générales, métiers de l’alimentation}} 1886 (fr-FR).}

\hypertarget{citeproc_bib_item_18}{[18] René Lespinasse \& François Bonnardot, \textit{\href{https://archive.org/details/lesmtiersetcor00boiluoft}{Les métiers et corporations de la ville de Paris. XIIIe siècle. Le Livre des métiers d’Étienne Boileau}} 1879 (fr-FR).}

\hypertarget{citeproc_bib_item_19}{[19] Osmund Lewry, « Grammar, Logic and Rhetoric 1220-1320 » Jeremy Catto (éd.) \textit{The History of the University of Oxford. The Early Oxford Schools}. 1984 (en-US).}

\hypertarget{citeproc_bib_item_20}{[20] \textit{Richard of Wallingford: an edition of his writings. The Life of Richard of Wallingford. Introductions and Commentaries to the Texts} John David North (éd.). 1976.}

\hypertarget{citeproc_bib_item_21}{[21] Olaf Pedersen, « The Corpus Astronomicum and the Traditions of Medieval Latin Astronomy » Owen Gingerich \& Jerzy Dobrzycki (éd.) \textit{Colloquia Copernicana III}. 1973 (en-US).}

\hypertarget{citeproc_bib_item_22}{[22] Emmanuel Poulle, \textit{Les instruments de la théorie des planètes selon Ptolémée: Équatoires et horlogerie planétaire du XIIIe au XVIe siècle} 1980.}

\hypertarget{citeproc_bib_item_23}{[23] Hastings Rashdall, \textit{\href{https://archive.org/details/universitieseur00unkngoog}{The Universities of Europe in the Middle Ages. Salerno, Bologna, Paris}} 1895 (en-US).}

\hypertarget{citeproc_bib_item_24}{[24] Olga Weijers, \textit{\href{https://doi.org/10.1484/M.SA-EB.5.106199}{La ’disputatio’ à la Faculté des arts de Paris (1200-1350 environ): Esquisse d’une typologie}} 1995 (fr-FR).}

\begin{figure}[htbp]
\centering
\includegraphics[height=.6\paperheight]{../img/Paris-BNF-Latin_15170-f126r.jpg}
\caption{Paris, BNF, Latin 1517, f.126r}
\end{figure}
\end{frame}
\end{document}