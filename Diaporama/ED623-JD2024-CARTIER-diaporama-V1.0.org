# -*- org-confirm-babel-evaluate: nil; -*-
#+title: Enseignement instrumenté des mathématiques: le cas de Richard de Wallingford
#+date: <2024-03-13>
#+language: fr
#+bibliography: ../bibliography.bib
#+cite_export: csl ../author-date.csl
#+email:

#+EXPORT_FILE_NAME: ED623-JD2024-CARTIER-diaporama-V1_1

#+beamer_header: \institute{SPHERE\\ ED 623: Savoirs, Sciences, Éducation}

#+startup: beamer
#+latex_class: beamer

#+beamer_header: \usepackage{caption}
#+beamer_header: \usepackage{subcaption}

#+options: toc:nil

#+beamer_frame_title: 3
#+beamer_frame_level: 3
#+beamer_header: \usetheme{upcite}
#+beamer_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Plan}\tableofcontents[currentsection, currentsubsection]\end{frame}}

#+latex_header: \newcommand{\fname}[1]{\textsc{#1}}

* Qu'est-ce que l'Université d'Oxford

*** [cite:@rashdallUniversitiesEuropeMiddle1895;p.6]    :B_frame:noexport:
:PROPERTIES:
:BEAMER_env: frame
:END:

***** Original quote                                      :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
The word /Universitas/ is one to which a false explanation is often assigned for polemical purposes by controversial writers […]. *The notion that a University means a /Universitas Facultatum/ --- a School in which all the Faculties or Branches of knowledge are represented ---* has, indeed, long since disappeared from the pages of professed historians; but it *is still persistently foisted upon the public by writers with whom history is subordinate to what may be called intellectual edification*.
[cite:@rashdallUniversitiesEuropeMiddle1895;p.6]
#+end_quote

***** Ma traduction                                       :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
Une fausse explication est souvent attribuée au mot /universitas/ à des fins polémiques par des auteurs ou autrices controversé⋅es […]. *L'idée que l'université signifie /Universitas Facultatum/ --- une école dans laquelle toutes les facultés ou branches de la connaissance sont représentées ---* a en effet, depuis longtemps disparu des pages des historien⋅nes respecté⋅es ; mais elle *est encore régulièrement présentée au public par des écrivain⋅es pour qui l'histoire est subordonnée à ce que l'on pourrait appeler de l'édification intellectuelle*.
#+end_quote

*** [cite:@rashdallUniversitiesEuropeMiddle1895;p.7]              :B_frame:noexport:
:PROPERTIES:
:BEAMER_env: frame
:END:

***** Original quote                                      :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
A glance into any collection of medieval documents reveals the fact that *the word "University" means merely a number, a plurality, an aggregate of persons*. /Universitas vestra/, in a letter addressed to a body of persons, means merely "the whole of you"; in a more technical sense *it denotes a legal corporation* […].
[cite:@rashdallUniversitiesEuropeMiddle1895;p.7]
#+end_quote

***** Ma traduction                                       :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
La lecture de n'importe quelle collection de documents médiévaux révèle le fait que *le mot "université" signifie seulement un ensemble, une pluralité, un aggrégat de personnes*. /Universitas vestra/, dans une lettre addressée à un groupe de personnes, signifie seulement "l'ensemble d'entre vous"; dans un sens plus techniques *il désigne une corporation légale*.
#+end_quote

*** Université et universel                                       :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

***** Hastings Rashdall (1858--1924)                              :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Hastings Rashdall (1858--1924), peinture à l'huile par Oswald Briley en 1923 (New College, 2755. Photographie: Bridgeman Images ©CC-BY)
#+ATTR_LATEX: :height .6\paperheight
[[../img/New_College-2755-painting-1923-BRILEY.jpg]]


***** [cite:@rashdallUniversitiesEuropeMiddle1895;p.6]    :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
The word /Universitas/ is one to which a false explanation is often assigned for polemical purposes by controversial writers […]. *The notion that a University means a /Universitas Facultatum/ --- a School in which all the Faculties or Branches of knowledge are represented ---* has, indeed, long since disappeared from the pages of professed historians; but it *is still persistently foisted upon the public by writers with whom history is subordinate to what may be called intellectual edification*.
#+end_quote


*** /Universitas/

***** [cite:@rashdallUniversitiesEuropeMiddle1895;p.7]                                      :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
A glance into any collection of medieval documents reveals the fact that *the word "University" means merely a number, a plurality, an aggregate of persons*. /Universitas vestra/, in a letter addressed to a body of persons, means merely "the whole of you"; in a more technical sense *it denotes a legal corporation* […].
#+end_quote

***** Seau de l'université                                        :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:
#+CAPTION: Sceau de l'université de Paris accordé en 1246. On y lit /Universitas magistrorium et scolarium/ (Paris, Bibliothèque Nationale de France, Monnaies et médailles antiques, MATR 119 - ©Etalab)
[[../img/Paris-BNF-Monnaies-MATR_119.jpg]]

*** Les corporations                                            :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

***** Colonnes                                                :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:
******* Livre des métiers                                       :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

Étienne Boileau, /Le Livre des métiers/ (c. 1266-1269) - voir [cite:@lespinasseMetiersCorporationsVille1879;@lespinasseMetiersCorporationsVille1886]:

#+CAPTION: "C'est l'ordonnance de l'intitulement des registres des métiers et marchandises de la ville de Paris qui sont escripts en ce livre par chapitres et nombres et aussi sont nombres les foilles du livres pour ce que chascun mestier [et] marchandise nest pas aparsoy ancoirs est en plusiers feuillez [et] parties." ( Paris, Bibliothèque Nationale de France, Français 24069, f. e recto - ©Etalab)
[[../img/Paris-BNF-Fr_24069-fer.jpg]]

******* Blasons de corporations dans la galerie de l'Hôtel de Ville de Paris :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Blasons de corporations à l'entrée du salon des arcades de l'hôtel de ville de Paris par Gabriel Léglise, réalisés en 1935 (Image: Pierre André Leclercq sur Wikimedia commons, 2011, ©CC-BY-SA)
#+ATTR_LATEX: :height .3\paperheight
[[../img/Wikimedia-Paris_Hotel_de_Ville_la_galerie_des_glaces_(2)-PIERRE_ANDRE_LECLERCQ-2011.jpg]]

***** Références
- [cite:@branthomeIntroductionHistoriographieCorporations2013] [cite:@jehannoTravailAuMoyen2015] [cite:@anheimHierarchiesTravailArtisanal2013]
#+beamer: \pause
- [cite:@legoffPourAutreMoyen1977] [cite:@braunsteinTravailEntrepriseAu2003]

*** Sources                                                       :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

***** Anciennes sources                                       :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:
******* Statuts                                                 :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:
#+CAPTION: Statuts --- ici ceux de l'université de Paris promulgués par Rorbert de Courçon en 1215 (Paris, Bibliothèque interuniversitaire de la Sorbonne, MSAUC 7 D.10.b) - voir [cite:@genetRapportTableRonde1997; @gibsonStatutaAntiquaUniversitatis1931]
#+ATTR_LATEX: :height .4\paperheight
[[../img/Paris-BIS-MSAUC_7_D10b.jpg]]

******* Manuels                                                 :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:
#+CAPTION: Raoul le Breton, /Questiones mathematice/ (Paris, Bibliothèque Nationale de France - Latin 16609, f.30r) - voir [cite:@weijersDisputatioFaculteArts1995;@lafleurTextesDidascaliquesIntroductions1997]
#+ATTR_LATEX: :height .4\paperheight
[[../img/Paris-BNF-Latin_16609-f30r.jpg]]

***** Mais aussi...                                             :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
******* Table ronde                                             :BMCOL:
:PROPERTIES:
:BEAMER_col: .6
:END:
[cite:@genetRapportTableRonde1997]
- Archives: /Gesta Abbatum Monastrii Sancti Albani/, Meton College
- Bases de données prosopographiques / bibliographiques
- Manuscrits + éditions numériques

#+beamer: \pause
******* Autre                                                   :BMCOL:
:PROPERTIES:
:BEAMER_col: .3
:END:

- Iconographie
- Données archéologiques

* Cursus d'un étudiant Oxfonien

*** Enseignement élémentaire

***** Origines de Richard de Wallingford                        :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:


******* Paris-Oxford                                            :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+ATTR_LATEX: :height .6\paperheight
#+RESULTS: draw_paris_oxford

#+beamer: \pause

******* [cite:@northRichardWallingfordEdition1976b;p.95] :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
It is easier still to overlook the difficulty whith which a great many clerks read Latin and, /a fortiori/, thought in the language. […] Archbishop Pecham is found complaining that, despite Walter of Merton's well-laid schemes, *the rule of Latin conversation was being constantly broken at Merton College* ---perhaps a less culpable failure, but one which at least suggests that *Latin did not easily become a language for creative thought*. […] Not surprisingly, vernacular writings flowed in an everquickening stream from the ink-horns of the fourteenth-century, *occasionnaly with a sentence or two of apology, or even of hostility towards the artificial language of the Schools*.
#+end_quote

***** Enseignement élémentaire                                  :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Seb Falk                                                :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+ATTR_LATEX: :height .7\paperheight
[[../img/Falk-Light_Ages-cover.png]]


******* [cite:@falkLightAgesSurprising2020;p.43]                                  :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:BEAMER_env: block
:END:

#+begin_quote
No beginner's primer survives from the medieval period, but the mathematical texts that were popular in monasteries start from the assumption that their readers had already attained that elementary level.
#+end_quote

***** Monocorde                                    :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* [cite:@baillyRepresentationsInstrumentsMusique2011]                                                     :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Psaltérions de la cathédrale de Reims [cite:@baillyRepresentationsInstrumentsMusique2011]
#+begin_figure
#+begin_center
#+ATTR_LATEX: :width .3\textwidth :center
[[../img/Bailly-Psalterion-2011-B64.png]]
#+ATTR_LATEX: :width .3\textwidth :center
[[../img/Bailly-Psalterion-2011-B66.png]]
#+ATTR_LATEX: :width .3\textwidth :center
[[../img/Bailly-Psalterion-2011-B67.png]]
#+end_center
#+end_figure

******* Odon de Cluny, /Scriptores ecclesiastici de musica/     :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

********* /Scriptores ecclesiastici de musica/              :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
#+begin_quote
/Sicut magister omnes tibi litteras primum ostendit in tabula, ita et musicus omnes cantilenae voces in monochordo insinuat/.
[cite:@beaujouanParRaisonNombres1991;I-p.614]
#+end_quote

********* Ma traduction                                     :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:

#+begin_quote
Alors que le maître vous présente d'abord toutes les lettres dans un tableau, de même le musicien introduit toutes les notes du chant avec le monocorde.
#+end_quote

***** Main Guidonienne                                          :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Main Guidonienne                                        :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:
#+CAPTION: Main Guidonienne (Milan, Veneranda Biblioteca Ambrosiana, D 75 inf, f.c3v - pas d'information de licence)
[[../img/Milan-Veneranda_Biblioteca_Ambrosiana-D_75_inf-fc3v.jpg]]

******* Bede, /De temporum ratione/                             :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: (Paris, Bibliothèque Nationale de France, Latin 7418 - ©Etalab)
[[../img/Paris-BNF-La_7418-ff3v_4r.jpg]]

*** La faculté des arts

***** Gloucestershire College                                   :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Carte d'Oxford                                          :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Carte d'Oxford par K. J. Wass à partir d'un brouillon de Janet Cooper et des informations de Julian Munby [cite:@crossleyHistoryCountyOxford1979]
[[../img/map-oxford-1375-WASS.png]]

******* St Mary University Church                               :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Église universitaire Sainte Marie dans la High Street à Oxford (Image: David Nicholls sur flickr, mars 2013 ©CC-BY-NC)
[[../img/Flickr-Oxford_St_Mary-NICHOLLS-2013.jpg]]

***** Parcours à la faculté des arts                            :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Colonnes                                            :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:
********* Sophiste et bachelier                               :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Emploi du temps fictif de Richard de Wallingford en tant que sophiste de la Faculté des arts (1308--1311), puis bachelier après sa /déterminatio/ (1311--1314) -- voir [cite:@gibsonStatutaAntiquaUniversitatis1931;p.lxxxi][cite:@fletcherFacultyArts1984;pp.375-384]
[[../img/DATA-Gloucester-EDT-sophiste-octobre-V1_0.png]]

********* Illustration                                        :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Incipit de l'/Introduction au jugement des astres/ d'Ali al-Kabisi par Conrad Heingarter (Paris, Bibliothèque Nationale de France, Latin 7432, f.160r - ©Etalab)
#+ATTR_LATEX: :height .5\paperheight
[[../img/Paris-BNF-La_7432-f160r.jpg]]

# [cite:@rashdallUniversitiesEuropeMiddle1895;pp.438-453]
******* Arts libéraux                                         :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
- /Trivium/ :: Logique, Grammaire, Rhétorique [cite:@osmundlewryGrammarLogicRhetoric1984][cite:@rashdallUniversitiesEuropeMiddle1895;pp.433-435]
- /Quadrivium/ :: Arithmétique, Musique, Géométrie, Astronomie [cite:@lejbowiczDisciplinesQuadriviumAstronomie1997;@beaujouanQuadriviumFaculteArts1997]




***** Sophiste à la faculté de théologie                        :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Emploi du temps                                     :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:

********* Regent                                                :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Emploi du temps fictif de Richard de Wallingford après son retour à Oxford  (1317--1321)
[[../img/DATA-Gloucester-EDT-regent-octobre-V1_0.png]]

********* Sophiste                                              :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Emploi du temps fictif de Richard de Wallingford après ses quatre premières années d'étude de la Bible (1321--1323)
[[../img/DATA-Gloucester-EDT-sophiste_theologie-octobre-V1_0.png]]

******* References                                            :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:

[cite:@rashdallUniversitiesEuropeMiddle1895;pp.464-465][cite:@genetMutationEducationCulture1999;pp.220-222]

***** Bachelier à la faculté de théologie                       :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

******* Bachelier Cursor                                        :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Emploi du temps fictif de Richard de Wallingford après son /incipium/ (1323--1325), puis en tant que sente
[[../img/DATA-Gloucester-EDT-cursor-octobre-V1_0.png]]

******* Parcours à la faculté de théologie                      :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

- 1323 :: /incipium/, bachelier Cursor (1323-1325)
- 1325 :: /tentative/, bachelier Sententiaire (1325-1327)
- 1327 :: /incipium/, bachelier Formatus

* /Cursus astronomicum/

*** Textes de référence                                           :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:


***** Colonne                                                     :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:
[cite:@pedersenCorpusAstronomicumTraditions1973]

******* Sacrobosco, Traité de la Sphère                       :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:

********* Climats                                               :BMCOL:
:PROPERTIES:
:BEAMER_col: .4
:END:
#+CAPTION: Représentation des climats dans le traité de la Sphère (Paris, BNF, NAL 1893, f.19r - ©Etalab)
[[../img/Paris-BNF-NAL_1893-f19r.jpg]]

********* Éclipses                                              :BMCOL:
:PROPERTIES:
:BEAMER_col: .4
:END:
#+CAPTION: Théorie des éclipses (Paris, BNF, NAL 1893, f.22v)
[[../img/Paris-BNF-NAL_1893-f22v.jpg]]

***** Instruments                                                 :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

******* Divers                                              :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:
********* Quadrant                                            :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Quadrant moderne (Paris, BNF, NAL 1893, ff.44v-45r)
#+ATTR_LATEX: :height .2\paperheight
[[../img/Paris-BNF-NAL_1893-ff44v_45r.jpg]]

********* Astrolabe                                           :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Construction de l'astrolabe (Paris, BNF, NAL 1893, ff.49v-50r)
#+ATTR_LATEX: :height .2\paperheight
[[../img/Paris-BNF-NAL_1893-ff49v_50r.jpg]]

******* Campanus, Theorice planetarum                         :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:

********* Theorie                                             :BMCOL:
:PROPERTIES:
:BEAMER_col: .4
:END:

#+CAPTION: Théorie de la Lune (Paris, BNF, NAL1893, f.66r)
#+ATTR_LATEX: :height .2\paperheight
[[../img/Paris-BNF-NAL_1893-f66r.jpg]]

********* Instrument                                          :BMCOL:
:PROPERTIES:
:BEAMER_col: .4
:END:

#+CAPTION: Équatoire pour la Lune [cite:@poulle-equatoires]
#+ATTR_LATEX: :height .2\paperheight
[[../img/POULLE-Equatoires2-1980-fig10-p894.png]]

*** Astrologie                                                    :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

***** Applications pratiques                                    :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Illustration de l'homme zodiacal dans Michael Scot, /Liber introductorius/ (Munich, Bayerische Staatsbibliothek, Clm 10268, f.94v - ©CC-BY-SA)
#+ATTR_LATEX: :height .6\paperheight
[[../img/Munich-BSB-Clm10268-f94v.png]]

***** Cours royales                                             :BMCOL:
:PROPERTIES:
:BEAMER_col: .5
:END:

#+CAPTION: Incipit du /Quadripartitum/ de Ptolémé par Conrad Hemgarter (Paris, Bibliothèque Nationale de France, Latin 7432, f.103v - ©Etalab)
#+ATTR_LATEX: :height .6\paperheight
[[../img/Paris-BNF-La_7432-f103v.jpg]]

* Bibliographie

*** Bibliographie                                                 :B_frame:
:PROPERTIES:
:BEAMER_OPT: allowframebreaks
:BEAMER_env: frame
:END:

#+print_bibliography:

#+CAPTION: Paris, BNF, Latin 1517, f.126r
#+ATTR_LATEX: :height .6\paperheight
[[../img/Paris-BNF-Latin_15170-f126r.jpg]]

* Annexes :noexport:


*** Source code for drawing maps
#+NAME: draw_map
#+begin_src python :session :exports both
  """ Plotting locations on a map """

  import geocoder
  import matplotlib.pyplot as plt
  import cartopy
  from cartopy.mpl.geoaxes import GeoAxes

  Place = tuple[str, float, float] # Name, longitude, latitude
  Legend = tuple[
      str,                         # Text of the legend
      float,                       # Text's x coordinate
      float,                       # Text's y coordinate
      float,                       # Point's x coordinate
      float,                       # Point's y coordinate
  ]
  Position = tuple[float, float]  # x, y

  MAP_MARGIN_X = 1              # Distance from western point to map's border
  MAP_MARGIN_Y = 1              # Distance from southern point to map's border
  LEGENDS_DEFAULT_OFFSET_X = .5 # Distance from a point to it's legend (x)
  LEGENDS_DEFAULT_OFFSET_Y = .5 # Distance from a point to it's legend (y)
  LEGENDS_OFFSET_MARGIN_Y = .1  # Distance between two legends

  def draw_map(places_names: list[str], filename: str, meridian: float = 159) -> str:
      ax = init_map(meridian)
      places = [get_place_informations(name, meridian) for name in places_names]
      draw_places(places, ax)
      plt.tight_layout()
      plt.savefig(filename)
      return filename

  def init_map(meridian: float = 159) -> GeoAxes:
      ax = plt.axes(projection=cartopy.crs.PlateCarree(central_longitude=meridian))
      ax.coastlines()
      ax.add_feature(cartopy.feature.OCEAN)
      ax.add_feature(cartopy.feature.LAKES)
      ax.add_feature(cartopy.feature.RIVERS)
      return ax

  def get_place_informations(name: str, meridian: float = 180) -> tuple[str, float, float]:
      geonames_id = geocoder.geonames(name, key=geonames_key).geonames_id
      data = geocoder.geonames(geonames_id, method="details", key=geonames_key)
      longitude = float(data.lng)
      return (data.address, longitude, float(data.lat))

  def draw_places(places: list[Place], ax: GeoAxes):
      legends = []
      for place in places:
          draw_place(place, ax)
          compute_legend_position(place, legends)
      for legend in legends:
          draw_legend(legend, ax)

  def draw_place(place: Place, ax: GeoAxes):
      ax.plot([place[1]], [place[2]], marker="o", color="red", markersize=4, transform=cartopy.crs.PlateCarree())
      # Make sure to have margins
      ax.plot([place[1] + MAP_MARGIN_X], [place[2] + MAP_MARGIN_Y], transform=cartopy.crs.PlateCarree())
      ax.plot([place[1] - MAP_MARGIN_X], [place[2] - MAP_MARGIN_Y], transform=cartopy.crs.PlateCarree())

  def draw_legend(legend: Legend, ax: GeoAxes):
      ax.annotate(
          legend[0],
          xytext=(legend[1], legend[2]),
          xy=(legend[3], legend[4]),
          arrowprops=dict(arrowstyle="->"),
          fontsize=12, wrap=True,
          horizontalalignment="center",
          transform=cartopy.crs.PlateCarree(),
      )

  def compute_legend_position(place: Place, other_legends: list[Legend]):
      x = place[1] + LEGENDS_DEFAULT_OFFSET_X
      y = compute_legend_y((x, place[2]), other_legends)
      other_legends.append((place[0], x, y, place[1], place[2]))

  def compute_legend_y(pos: Position, other_legends: list[Legend]) -> float:
      offset = LEGENDS_DEFAULT_OFFSET_Y
      for other in other_legends:
          offset = avoid_conflict(pos, offset, other)
      return pos[1] + offset

  def avoid_conflict(pos: Position, offset: float, other: Legend) -> float:
      new_pos = (pos[0], pos[1] + offset)
      if are_legends_conflicting(new_pos, other):
          return offset * -1
      return offset

  def are_legends_conflicting(pos: Position, other: Legend) -> bool:
      return LEGENDS_OFFSET_MARGIN_Y > abs(pos[1] - other[2])

  def is_above(pos: tuple[float, float], other: Legend) -> bool:
      return pos[1] >= other[2]

#+end_src

#+header: :noweb strip-export
#+NAME: draw_paris_oxford
#+begin_src python :results value file :var matplot_lib_filename=(org-babel-temp-file "local_map" ".png") :var geonames_key="catgolin" :exports both
  <<draw_map>>
  MAP_MARGIN_X = 1
  MAP_MARGIN_Y = 1
  LEGENDS_DEFAULT_OFFSET_X = 0
  LEGENDS_DEFAULT_OFFSET_Y = .6
  LEGENDS_OFFSET_MARGIN_Y = .5

  places = ["Oxford, England", "Paris", "St Albans", "Lincoln, England", "London", "Wallingford, Oxfordshire"]
  return draw_map(places, matplot_lib_filename)
  #+end_src
