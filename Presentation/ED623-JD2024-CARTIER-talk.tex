% Created 2024-03-25 lun. 21:06
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage{setspace}
\onehalfspace
\usepackage[LAE, LGR, T2A, T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage[french]{babel}
\usepackage{imakeidx}
\usepackage{textalpha}
\usepackage{arabtex}
\usepackage{utf8}
\usepackage[newfloat]{minted}
\usepackage[hidelinks]{hyperref}
\usepackage{wasysym}
\usepackage{setspace}
\usepackage{marginnote}
\usepackage{fmtcount}
\usepackage{float}
\floatstyle{boxed}
\restylefloat{figure}
\doublespacing
\newcommand{\fname}[1]{\textsc{#1}}
\newcommand{\displaytime}[1][0]{
\addtocounter{time}{#1}
\setcounter{minutes}{\value{time}}
\divide\value{minutes} by 60
\setcounter{minutesinseconds}{\value{minutes}}
\multiply\value{minutesinseconds} by 60
\setcounter{seconds}{\value{time}}
\addtocounter{seconds}{-\value{minutesinseconds}}
\marginpar{\textit{(\padzeroes[2]{\decimal{minutes}}'\padzeroes[2]{\decimal{seconds}})}}
}
\author{Clément Cartier\thanks{contact@catgolin.eu}}
\date{\textit{<2024-03-13 mer.>}}
\title{Enseignement instrumenté des mathématiques: le cas de Richard de Wallingford}
\hypersetup{
 pdfauthor={Clément Cartier},
 pdftitle={Enseignement instrumenté des mathématiques: le cas de Richard de Wallingford},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.6-pre)}, 
 pdflang={French}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tableofcontents
\newpage
\newcounter{time}
\newcounter{minutes}
\newcounter{seconds}
\newcounter{minutesinseconds}
\setcounter{time}{0}

\section{Introduction}
\label{sec:org89deeda}

Dans le cadre de ma thèse, je m'intéresse aux instruments qui sont décrits en latin entre le \textsc{xiii}e et le \textsc{xvi}e siècle dans le cadre de calculs liés aux éclipses.
Le but de ce travail est de réfléchir aux liens entre les procédures et les matériels de calcul, et à l'histoire du calcul instrumenté.
\displaytime[15]

Je suis seulement au début de ce travail, donc je ce que je vais vous présenter aujourd'hui est plus un état des lieux qu'autre chose, mais je voulais profiter de cette journée doctorale dans l'espoir d'ouvrir une discussion avec des personnes qui pourraient s'intéresser au calcul instrumenté sous d'autres perspectives; notamment en lien avec des problématiques contemporaines.
\displaytime[15]

Puisque cette école doctorale regroupe des personnes d'horizons assez variés, je voudrais essayer de rendre cette présentation un peu interactive avec l'aide de l'application Wooclap, à laquelle vous pouvez vous connecter en utilisant le QRcode ou le lien qui s'affiche à l'écran. J'aurai quelques questions à vous poser pendant la présentation auxquelles vous pourrez répondre par ce biais ---donc gardez ce lien ouvert pendant la présentation.
\displaytime[30]

Le corpus que j'étudie comporte des textes de plusieurs auteurs, rattachés à différentes universités. Pour aujourd'hui je voudrais me concentrer sur le texte le plus commenté de mon corpus: celui de l'\emph{Albion} de Richard de Wallingford, composé à l'université d'Oxford en 1326.
\displaytime[15]

On sait que Richard de Wallingford a obtenu la licence de la faculté des arts à Oxford en 1314, et qu'il y a été actif entre 1317 et 1327 avant d'obtenir la licence de la faculté de théologie et de devenir abbée de Saint-Albans.
L'\emph{Albion} est donc composé pendant cette période où Richard de Wallingford était à l'université d'Oxford, et la question que je voudrais explorer est celle du lien que peut avoir cet instrument avec l'enseignement dispensé à la faculté des arts pendant cette période.
\displaytime[30]

Mais avant d'en venir à ce point, j'ai pensé qu'il serait utile de reprendre un bref apperçu de ce que nous connaissons des enseignements qui étaient dispensés dans les universités du \textsc{xiv}e siècle.
Pour ce faire, comme je n'ai que peu de temps aujourd'hui, je voudrais vous mettre à contribution de vous demander ce que vous évoque l'étymologie du terme \og Université\fg{}, qui vient du latin \emph{universitas}.
\displaytime[60]

\section{L'université d'Oxford}
\label{sec:org18ba618}

\subsection{Qu'est-ce qu'une université?}
\label{sec:orgc7b8f60}
\textbf{[Si les réponses parlent d'universalité]}

À la fin du \textsc{xix}e siècle, Hasting Rashdall regrettait le fait que beaucoup de ses contemporains véhiculaient l'idée selon laquelle, l'étymologie de l'\og Université\fg{} renverrait à une ambition universaliste, de rassemblement de tous les savoirs.
On n'a pourtant aucun élément solide qui nous permettrait d'attribuer une quelconque ambition d'universalité aux \emph{universitas} du \textsc{xiv}e siècle.
\displaytime[20]

\begin{figure}
\begin{quote}
The notion that a University means a \emph{Universitas Facultatum} --- a School in which all the Faculties or Branches of knowledge are represented --- […] is still persistently foisted upon the public by writers with whom history is subordinate to what may be called intellectual edification.
(\citeprocitem{12}{Rashdall, 1895} p.6)
\end{quote}
\caption{Citation d'Hasting Rashdall}
\end{figure}

En fait, dans sa grande étude sur les universités en Europe au Moyen Âge (\emph{The Universities of Europe in the Middle Ages}), Hasting Rashdall note que ce qu'on désigne aujourd'hui comme \og université\fg{} ressemble moins à ce que les médiévaux désignent par le terme de \emph{Studium} qu'à ce dont il est question quand on trouve le terme \emph{universitas}.
On comprendrait très mal les textes et les pratiques de cette époque en essayant d'y plaquer les cadres qui nous sont familiers aujourd'hui.
\displaytime[30]

\textbf{[Selon les réponses]}

Avant le \textsc{xii}e siècle, le terme \emph{universitas} ne s'emploie pas seul: il sert à indiquer qu'on s'adresse à l'ensemble d'un groupe.
\emph{Universitas vestras} signifie \og vous tou⋅tes\fg{}, et \emph{universitas bononiensis} désigne la communauté des citoyen⋅nes de Bologne plutôt que son université.
Les choses changent un peu à partir du \textsc{xiii}e siècle, lorsque les termes \emph{societas}, \emph{communitas} et \emph{universitas} sont employés de manière interchangeables dans des textes juridiques pour traiter de l'organisation de différentes communautés citoyennes et professionnelles, en particulier pour établir les statuts des corporations de métiers ou des \og guildes\fg{}.
\displaytime[30]

C'est dans ce cadre de réglementation des communautés professionnelles urbaines qu'il faut comprendre le terme \emph{universitas}.
Ce qui m'amène à vous mettre de nouveau à contribution pour savoir si la notion de corporation vous évoque quelque chose\footnote{Surtout pour capter l'attention de l'auditoire parce que je me doute que non.}.
\displaytime[30]

\textbf{[Selon les réponses]}

L'historiographie des corporations professionnelles du Moyen Âge tardif est en grande partie tribuaire d'auteurs qui, dans la seconde moitié du \textsc{xix}e siècle et dans la première moitié du \textsc{xx}e siècle, ont construit leur récit à partir de textes juridiques parisiens de la deuxième moitié du \textsc{xiii}e siècle (\citeprocitem{1}{Anheim, 2013}, \citeprocitem{3}{Branthôme, 2013}, \citeprocitem{7}{Jéhanno, 2015}).
\displaytime[30]

La source paradigmatique de ce travail est le répertoire des statuts des métiers de Paris établi entre 1266 et 1269 par Étienne Boileau, prévôt de Paris, et édité par Rene Lespinasse et François Bonnardot en 1879, à partir notamment d'un manuscrit légué par le Cardinal Richelieu à la Bibliothèque de la Sorbonne (\citeprocitem{10}{Lespinasse \& Bonnardot, 1879}).
\displaytime[30]

On peut en retenir que, au moins à partir du \textsc{xiii}e siècle, il y a des textes qui tentent de contrôler l'exercice des métiers à Paris en instituant des communautés réglées.
Ainsi, pour exercer en tant que charcutier à Paris, il faut apprendre le métier pendant quatre ans auprès d'un maître appelé prud'homme, puis payer un brevet de 2 sols et prêter serment pour devenir chef d'œuvre (\citeprocitem{9}{Lespinasse, 1886} p.317). Les chefs d'œuvre continuent d'exercer sous la direction d'un prud'homme jusqu'à ce que leur compétence soit reconnue par les jurés de la corporation, élus parmi les prud'homme, qui peuvent décider de l'accepter comme prud'homme en échange du paiement de 20 sols\footnote{\og Item que doresnnavant nul homme ne pourra estre maistre saulsissier et charcuitier, cuire char, faire saulcisses, ne tenir ouvrouer, ne fenestre ouverte a Paris, s'il na esté quatre ans apprentiz a maistre dudit mestier, a Paris, et fait chef d'euvre, ou s'il n'est expert oudit mestier et tel rapporté par les jurez et fait chef d'euvre, comme dessus, et que pour son entrée de maistre, il ait paié vingt solz parisis, c'est assavoir, dix solz au Roy, cinq solz a la confrairie dudit mestier et cinq solz aux jurez; excepté les filz de maistre, nez et procreez en loial mariage, qui seront reçeuz a estre maistre oudit mestier, sans faire aucun chef d'euvre, ne avoir esté apprentiz, en paiant seulement vingt solz parisis, a apliquer comme dessus\fg{} (\citeprocitem{9}{Lespinasse, 1886} p.321)}.
Ainsi, nul n'est supposé pouvoir exercer le métier de charcutier sans faire partie de la corporation, et la qualité du travail est constamment soumis au contrôle des jurés.
\displaytime[30]

L'\emph{universitas magistrorum et scolarium} apparaît ainsi surtout comme la corporation des enseignants et des élèves d'une localité, organisée sur le même modèle que les communautés de métier.
Pour enseigner à Paris, il faut apprendre un certain nombre d'années auprès d'un maître avant de devenir \og bachelier\fg{} pour enseigner ses premières leçons tout en continuant d'étudier, jusqu'à enfin obtenir la licence d'enseignement qui permet d'ouvrir sa propre école, régulièrement contrôlée par les membres de l'université.
\displaytime[30]

Cependant, les statuts des corporations qui nous sont parvenus, en plus d'être parcelaires, ne représentent pas nécessairement la réalité des pratiques du \textsc{xiv}e siècle.
Depuis les années 1990, dans la continuité notamment de Jacques Le Goff (\citeprocitem{8}{Le Goff, 1977}) ou de Philippe Braunstein (\citeprocitem{4}{Braunstein, 2003}), des études plus critiques, qui croisent les sources juridiques avec d'autres données prosopographiques, archéologiques, contractuelles, judiciaires, etc., donnent à voir un tableau beaucoup plus contrasté des différentes organisation du travail, beaucoup plus dynamiques et beaucoup moins uniformes qu'initialement envisagé (\citeprocitem{7}{Jéhanno, 2015}).
\displaytime[30]

De la même manière, pour les universités, il est clair que les statuts qui nous sont parvenus ne peuvent pas être pris pour une description fidèle du fonctionnement de chaque école, qui serait valable à toutes les époques.
Dans le cas de Richard de Wallingford, malheureusement, on dispose de relativement peu de sources fiables pour connaître son quotidien à l'université d'Oxford, et je ne suis encore qu'au début de mes recherches: ce que je vais vous présenter aujourd'hui repose donc encore en grande partie sur une interprétation des statuts, croisée avec les éléments biographiques dont on dispose.
\displaytime[30]

\section{Cursus d'un étudiant Oxfonien}
\label{sec:orga902bc0}
Le parcours de Richard de Wallingford à l'université d'Oxford est assez paradigmatique du cursus suivi par les universitaires à Paris et Oxford au début du \textsc{xiv}e siècle.
\displaytime[15]

\subsection{Enseignement élémentaire}
\label{sec:orgadc2dbd}

D'après les chroniques de l'abbaye de Saint-Albans, probablement composées en partie lorsque Richard de Wallingford en était l'abbée (\citeprocitem{11}{North, 1976} p.17), le père de notre auteur serait un forgeron, décédé alors que Richard avait environ 10 ans.
Richard de Wallingford aurait alors été adopté par le prêtre de la ville, William de Kirkeby (\citeprocitem{11}{North, 1976} p.1).
\displaytime[15]

Vu ces origines, je voudrais maintenant vous demander quelle est, ou quelles sont, à votre avis, la ou les langues que Richard de Wallingford pouvait parler au quotidien?
\displaytime[45]

\textbf{[Selon les réponses]}

On n'a en vérité aucune information sur les langues parlées par Richard de Wallingford. Les statuts de l'université et les règles de l'Église prescrivent le latin comme langue véhiculaire, surtout en théologie, et on a tendance encore aujourd'hui à accorder plus de crédit aux textes rédigés en latin plutôt que dans des langues vernaculaires.
Pourtant, au \textsc{xiv}e siècle, ça fait longtemps que personne n'a plus le latin comme langue maternelle.
On sait que le roi Edouard III avec qui Richard de Wallingford a beaucoup échangé, parle plutôt le normand que le latin, de même que certains clercs qui étudient au Merton College en même temps que notre auteur, en infraction des statuts de l'université.
Certains textes de Richard de Wallingford nous sont parvenus dans des traductions en Moyen anglais, d'autres manuscrits comportent des inscriptions en gaélique, et on sait que différentes variations de l'anglo-normand proches des langues picardes et de celles parlées y compris à Paris étaient parlées dans la région d'Oxford.
\displaytime[45]

\begin{figure}
\begin{quote}
It is easier still to overlook the difficulty whith which a great many clerks read Latin and, \emph{a fortiori}, thought in the language. […] Archbishop Pecham is found complaining that, despite Walter of Merton's well-laid schemes, \textbf{the rule of Latin conversation was being constantly broken at Merton College} ---perhaps a less culpable failure, but one which at least suggests that \textbf{Latin did not easily become a language for creative thought}. […] Not surprisingly, \textbf{vernacular writings flowed in an everquickening stream from the ink-horns of the fourteenth-century, occasionnaly with a sentence or two of apology, or even of hostility towards the artificial language of the Schools}.
(\citeprocitem{11}{North, 1976} p.95)
\end{quote}
\end{figure}

Même si j'ai trouvé très peu d'éléments concernant l'enseignement élémentaire dont Richard de Wallingford a pu bénéficier, on peut imaginer qu'il a pu apprendre à lire, à écrire et à compter dans sa langue natale avant d'apprendre le latin.
Depuis le règne de Guillaume le Conquérent, le prioré de Wallingford a été soustrait à l'autorité de l'évêque de Lincoln pour rejoindre l'abbaye bénédictine de Saint-Albans, qui possède des cellules dans toutes la province du Canterburry.
\displaytime[30]

On n'a pas de traces de l'enseignement élémentaire qui pouvait être dispensé là bas à cette époque, mais on sait que l'école du monastère de Saint-Albans jouissait d'une très grande réputation, et que l'abaye accordait une très grande importance à l'enseignement, non seulement religieux, mais aussi pratique avec notamment de nombreux textes d'astronomie en langue vernaculaire pour apprendre quand et comment pratiquer les travaux agricoles, faire commerce avec les communautés locales, connaîre l'heure des messes, etc.
\displaytime[30]

\begin{figure}
\begin{quote}
No beginner's primer survives from the medieval period, but the mathematical texts that were popular in monasteries start from the assumption that their readers had already attained that elementary level.
(\citeprocitem{5}{Falk, 2020} p.43)
\end{quote}
\end{figure}

En tant que fils adoptif du prieur, Richard de Wallingford aura probablement eu un enseignement plus soutenu que d'autres enfants, notamment en latin et en musique pour aider dans les offices religieux.
\displaytime[15]

On ne sait rien d'un éventuel intérêt de Richard de Wallingford pour la théorie musicale, mais s'il a reçu un enseignement en la matière, il aura probablement travaillé sur deux instruments dont l'usage est décrit par Guy Beaujouan pour le \textsc{xi}e siècle (\citeprocitem{2}{Beaujouan, 1991} I-pp.639-667).
\displaytime[15]

Premièrement, le monochorde, qu'Odon de Cluny présente comme l'alphabet musical:
\begin{quote}
Alors que le maître vous présente d'abord toutes les lettres dans un tableau, de même le musicien introduit toutes les notes du chant avec le monocorde.\footnote{\emph{Sicut magister omnes tibi litteras primum ostendit in tabula, ita et musicus omnes cantilenae voces in monochordo insinuat.} (\citeprocitem{2}{Beaujouan, 1991} I-p.641)}
\end{quote}
Il s'agit d'une sorte de guitare à une seule corde: en divisant la corde selon différents rapports, on obtient différentes hauteurs de notes. En jouant avec cet instrument ou avec des variations à plusieurs cordes sur lesquelles, on apprend à reconnaître les intervales musicaux et à déchiffrer les notations musicales
(\citeprocitem{2}{Beaujouan, 1991} I-pp.639-667).
\displaytime[30]

Deuxièmement, il y a la main, utilisée de nombreuses manières par les auteurs médiévaux pour aider la mémoire ou le calcul.
Un exemple célèbre est la main dite Guidonienne, qui permet, en associant les articulations de la main aux différents tons harmoniques, de mémoriser les notations apprises sur le monocorde pour les différents modes.
\displaytime[15]

Un autre exemple célèbre est une méthode de calcul proposée par Bède le vénérable dans son traité sur la mesure du temps, dans lequel il propose, d'une part un moyen de compter jusqu'à 9999, et d'autre part une méthode pour calculer la date de Pâques.
\displaytime[15]

Vu le peu de sources dont on dispose sur l'enseignement élémentaire, on ne peut pas dire si ces techniques particulières ont effectivement été enseignées au jeune Richard de Wallingford, mais il est probable qu'il ait appris, d'une façon ou d'une autre, à compter sur ses doigts et à reconnaître les notes sur un instrument pour savoir dire la messe avant d'arriver à Oxford.
\displaytime[30]

\subsection{La Faculté des Arts}
\label{sec:orgecc0178}

Le parcours universitaire de notre auteur commence lorsqu'il est âgé d'environ 18 ans.
Il s'installe dans le domaine de Gloucester, au Nord d'Oxford, légué par le barron John Giffard à l'abbaye de Saint-Albans une vingtaine d'années plus tôt pour y accueillir les étudiants de Saint-Albans.
\displaytime[30]

Au début du \textsc{xiv}e siècle, les ordres mendiants ne semblent pas bienvenus à Oxford, qui demandent au roi Edouard II d'intervenir pour modifier les statuts de l'université en leur faveur, contre l'avis du chancelier  (\citeprocitem{6}{Gibson, 1931} p.xli).
\displaytime[15]

La présence d'un monastère Carmélites entre Gloucester et le collège de Durham appartenant à une autre abbaye bénédictine est une autre source de tension: les bénédictins s'opposent à la modification de la règle de Saint Albert qui interdit aux Carmes de prendre part à la vie apostolique, et notamment de participer aux enseignements.
Les moines noirs ont alors pour tâche, en marge de leurs études, de surveiller les ermites blancs.
\displaytime[30]

De façon générale, les 
\displaytime[15]
\begin{quote}
Outside the north gate of Oxford, it was safely removed from the worst temptations and dangers of student life but close to the clean water of the winding River Thames. […] Alongside attending lectures and structured discussions, they were still expected to participate in divine offices, and to practise preaching in both Latin and English. Despite this, they clearly contrived to study widely and have active social lives.
(\citeprocitem{11}{North, 1976} p.117)
\end{quote}

\section{Bibliographie}
\label{sec:orgd443f2a}
\hypertarget{citeproc_bib_item_1}{[1] Étienne Anheim, « Les hiérarchies du travail artisanal au Moyen Âge entre histoire et historiographie » \textit{Annales. Histoire, Sciences Sociales}. 2013 (fr-FR: en ligne: \url{https://www.cairn.info/revue-annales-2013-4-page-1027.htm} - consulté le 22 mars 2024).}

\hypertarget{citeproc_bib_item_2}{[2] Guy Beaujouan, \textit{Par raison de nombres: l’art du calcul et les savoirs scientifiques médiévaux} 1991.}

\hypertarget{citeproc_bib_item_3}{[3] Thomas Branthôme, « Introduction à l’historiographie des corporations : une histoire polémique (1880-1945) » \textit{Les Études Sociales}. 2013 (fr-FR: en ligne: \url{https://www.cairn.info/revue-les-etudes-sociales-2013-1-2-page-213.htm} - consulté le 22 mars 2024).}

\hypertarget{citeproc_bib_item_4}{[4] Philippe Braunstein, \textit{Travail et entreprise au Moyen Âge} 2003.}

\hypertarget{citeproc_bib_item_5}{[5] Seb Falk, \textit{The light ages: the surprising story of medieval science} 2020.}

\hypertarget{citeproc_bib_item_6}{[6] \textit{Statuta antiqua universitatis oxoniensis} Strickland Gibson (éd.). 1931.}

\hypertarget{citeproc_bib_item_7}{[7] Christine Jéhanno, « Le travail au Moyen Âge, à Paris et ailleurs : retour sur l’histoire d’un modèle » \textit{Médiévales. Langues, Textes, Histoire}. 2015 (fr-FR: en ligne: \url{https://journals.openedition.org/medievales/7567} - consulté le 22 mars 2024).}

\hypertarget{citeproc_bib_item_8}{[8] Jacques Le Goff, \textit{Pour un autre Moyen Âge. Temps, travail et culture en Occident} 1977.}

\hypertarget{citeproc_bib_item_9}{[9] René Lespinasse, \textit{Les métiers et corporations de la ville de Paris : XIVe-XVIIIe siècles. Ordonnances générales, métiers de l’alimentation} 1886 (fr-FR: en ligne: \url{https://gallica.bnf.fr/ark:/12148/bpt6k5823044k}).}

\hypertarget{citeproc_bib_item_10}{[10] René Lespinasse \& François Bonnardot, \textit{Les métiers et corporations de la ville de Paris. XIIIe siècle. Le Livre des métiers d’Étienne Boileau} 1879 (fr-FR: en ligne: \url{https://archive.org/details/lesmtiersetcor00boiluoft}).}

\hypertarget{citeproc_bib_item_11}{[11] \textit{Richard of Wallingford: an edition of his writings. The Life of Richard of Wallingford. Introductions and Commentaries to the Texts} John David North (éd.). 1976.}

\hypertarget{citeproc_bib_item_12}{[12] Hastings Rashdall, \textit{The Universities of Europe in the Middle Ages. Salerno, Bologna, Paris} 1895 (en-US: en ligne: \url{https://archive.org/details/universitieseur00unkngoog}).}
\end{document}